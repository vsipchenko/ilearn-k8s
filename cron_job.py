import os


def job(path, name):
    with open(f"{path}/{name}", "a") as f:
        f.write("appended text from cron job")


if __name__ == '__main__':
    job(os.getenv("FILE_PATH"), os.getenv("FILE_NAME"))
