import os


def init():
    with open(f'{os.getenv("FILE_PATH")}/{os.getenv("FILE_NAME")}', 'w') as f:
        f.write('<h1>hello from template file</h1>')
    print('completed')


if __name__ == "__main__":
    init()
