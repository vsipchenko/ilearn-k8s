FROM python:3.7

COPY ./cron_job.py /usr/src/foo_cron_job/cron_job.py
WORKDIR /usr/src/foo_cron_job

CMD python cron_job.py