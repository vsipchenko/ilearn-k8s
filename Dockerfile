FROM python:3.7

COPY . /usr/src/foo_app
WORKDIR /usr/src/foo_app

RUN pip install -r requirements.txt

EXPOSE 5001

ENTRYPOINT flask run --host 0.0.0.0 --port 5001