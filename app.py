import os
import datetime
import logging

from flask import Flask

logging.basicConfig(filename="logs/foo.log", level=logging.INFO, format='%(message)s')

app = Flask(__name__)
logging.info('Server started')


@app.route('/')
def index():
    curr_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    pod_id = os.getenv("POD_ID")
    logging.info(f'{curr_time}: {pod_id or "local"} POD request received')

    return f'<h3>Hello from foo {curr_time}!</h3>'


@app.route('/asd')
def asd():
    return f'<h1>asd</h1> ' \
        f'<ul>' \
        f'<li>{os.getenv("POD_ID")}</li>' \
        f'<li>{os.getenv("DB_HOST")}</li>' \
        f'<li>{os.getenv("DB_PASS")}</li>' \
        f'<li>{os.getenv("FLASK_APP")}</li>' \
        f'</ul>'


@app.route('/test-init')
def test_init():
    with open(f'{os.getenv("FILE_PATH")}/{os.getenv("FILE_NAME")}', 'r') as f:
        content = f.readlines()
    return str(content)


if __name__ == '__main__':
    app.run()
