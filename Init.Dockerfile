FROM python:3.7

COPY ./init_script.py /usr/src/foo_init/init_script.py
WORKDIR /usr/src/foo_init

CMD python init_script.py